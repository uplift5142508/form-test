import React from 'react';
import eyeicon from './assets/eye.png';

function Text(props) {
  const {
    value,
    onChange,
    type,
    inputClass,
    id,
    labelFor,
    labelClassName,
    content,
    onClick = null,
    icon,
  } = props;

  const runner = () => {
    onClick();
  };

  return (
    <>
      <label htmlFor={labelFor} className={labelClassName}>
        {content}
      </label>
      <div className='col-sm-12 input-group'>
        <input
          type={type}
          className={inputClass}
          id={id}
          onChange={onChange}
          value={value}
        />
        {icon && (
          <button className='input-group-text' onClick={runner} type='button'>
            <img src={eyeicon} alt='eye icon' />
          </button>
        )}
      </div>
    </>
  );
}

export default Text;
