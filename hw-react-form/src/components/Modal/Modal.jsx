import React from 'react';
import mars from './assets/marcel.png';
import billy from './assets/billy.png';
import marj from './assets/marj.png';
import alli from './assets/alli.png';
import './Modal.css';

function Modal({ onClick }) {
  return (
    <>
      <div
        className='modal'
        id='exampleModal'
        aria-labelledby='exampleModalLabel'
        aria-hidden='true'
        style={{ display: 'block', background: 'rgba(0, 0, 0, 0.7)' }}
      >
        <div className='modal-dialog modal-dialog-centered'>
          <div className='modal-content'>
            <div className='modal-header'>
              <h1
                className='modal-title fs-5  font-monospace'
                id='exampleModalLabel'
              >
                Aisha Tech
              </h1>
              <button
                type='button'
                className='btn-close'
                data-bs-dismiss='modal'
                aria-label='Close'
                onClick={() => {
                  onClick(false);
                }}
              ></button>
            </div>
            <div className='modal-body'>
              <h1>Registration Successful</h1>
              <h1 className='text-center  font-monospace'>The A team</h1>
              <img src={mars} className='modal-img' />
              <img src={billy} className='modal-img' />
              <img src={marj} className='modal-img' />
              <img src={alli} className='modal-img' />
            </div>
            <div className='modal-footer'>
              <button
                type='button'
                className='btn btn-secondary'
                data-bs-dismiss='modal'
                onClick={() => {
                  onClick(false);
                }}
              >
                Close
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default Modal;
