import React from 'react';
import './Form.css';
import { useState } from 'react';
import { useEffect } from 'react';

function Form() {
  const [username, setUserName] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [isValid, setIsValid] = useState({
    valid: null,
    match: null,
    usernameFound: null,
  });

  const registeredUsers = [
    { username: 'Juan' },
    { username: 'upliftcodecamp' },
  ];

  const usernameHandler = (event) => {
    setUserName(event.target.value);
  };

  const passwordHandler = (event) => {
    const passwordInput = event.target.value;
    const passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!@#?]).{8,}$/;
    const passwordCheck = passwordRegex.test(password);
    const confirmPassCheck = passwordRegex.test(confirmPassword);

    if (event.target.id === 'password') {
      setPassword(passwordInput);
    } else {
      setConfirmPassword(passwordInput);
    }

    if (passwordCheck && confirmPassCheck) {
      console.log('pass check');
      setIsValid({ ...isValid, valid: true });
    } else {
      console.log('pass check else');
      setIsValid({ ...isValid, valid: false });
    }
  };

  const formSubmit = (event) => {
    event.preventDefault();

    console.log(password, ' === ', confirmPassword);
    if (password === confirmPassword) {
      console.log('pass check match');
      setIsValid({ ...isValid, match: true });
      console.log(isValid);
    } else {
      console.log('pass check match');
      setIsValid({ ...isValid, match: false });
    }

    registeredUsers.forEach((el) => {
      el.username === username
        ? setIsValid({ ...isValid, usernameFound: true })
        : setIsValid({ ...isValid, usernameFound: false });
    });

    console.log(isValid);
    if (!isValid.usernameFound && isValid.match) {
      console.log('first');
      setConfirmPassword('');
      setUserName('');
      setPassword('');
    }
  };

  useEffect(() => {}, [password, confirmPassword, isValid]);

  return (
    <form
      className='form needs-validation shadow border border-dark'
      onSubmit={formSubmit}
    >
      <h1 className='text-center mb-4 font-monospace'>REGISTRATION PAGE</h1>
      <div className='row mb-3 '>
        <label htmlFor='inputEmail3' className='col-form-label'>
          Username
        </label>
        <div className='col-sm-12'>
          <input
            type='text'
            className='form-control'
            id='inputEmail3'
            onChange={usernameHandler}
            value={username}
            required
          />
        </div>

        <div className='invalid-feedback d-block'>
          {isValid.usernameFound
            ? 'Username is taken'
            : username.trim() === '' &&
              'Please enter valid username/password cannot be blank'}
        </div>
      </div>

      <div className='row mb-3'>
        <label htmlFor='password' className='col-form-label'>
          Password
        </label>
        <div className='col-sm-12'>
          <input
            type='password'
            className='form-control'
            id='password'
            onChange={passwordHandler}
            value={password}
            required
          />
          <div className='invalid-feedback d-block'>
            {!isValid.valid
              ? ' Please enter valid username/password cannot be blank'
              : password.trim() !== '' && ''}
          </div>
        </div>
      </div>

      <div className='row mb-3'>
        <label htmlFor='confirmPassword' className='col-form-label'>
          Confirm Password
        </label>
        <div className='col-sm-12'>
          <input
            type='password'
            className='form-control'
            id='confirmPassword'
            onChange={passwordHandler}
            value={confirmPassword}
            required
          />

          {!isValid.valid && (
            <div className='invalid-feedback d-block'>
              Please enter valid username/password cannot be blank
            </div>
          )}

          {!isValid.match && isValid.valid ? (
            <div className='invalid-feedback d-block'>
              Those passwords didn't match. Please try again.
            </div>
          ) : (
            isValid.match && isValid.isValid && ''
          )}
        </div>
      </div>

      <div className='d-grid gap-2 mt-5'>
        <button type='submit' className='btn btn-dark btn-lg'>
          REGISTER
        </button>
      </div>
    </form>
  );
}

export default Form;

/* 
import React from 'react';
import './Form.css';
import { useState } from 'react';
import { useEffect } from 'react';

function Form() {
  const [username, setUserName] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [isUsernameFound, setIsUsernameFound] = useState(false);
  const [isPasswordValid, setIsPasswordValid] = useState(false);
  const [passwordMatch, setPasswordMatch] = useState(false);

  const registeredUsers = [
    { username: 'Juan' },
    { username: 'upliftcodecamp' },
  ];

  const usernameHandler = (event) => {
    setUserName(event.target.value);
  };

  const passwordHandler = (event) => {
    const passwordInput = event.target.value;
    const passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!@#?]).{8,}$/;
    const passwordCheck = passwordRegex.test(passwordInput);


    if (event.target.id === 'password') {
      setPassword(passwordInput);
    } else {
      setConfirmPassword(passwordInput);
    }

    if (passwordCheck) {
      
    } else {

    }
  };

  const formSubmit = (event) => {
    event.preventDefault();

    console.log(password, ' === ', confirmPassword);
    if (password === confirmPassword) {
      console.log('pass check match');
      setIsValid({ ...isValid, match: true });
      console.log(isValid);
    } else {
      console.log('pass check match');
      setIsValid({ ...isValid, match: false });
    }

    registeredUsers.forEach((el) => {
      el.username === username
        ? setIsValid({ ...isValid, usernameFound: true })
        : setIsValid({ ...isValid, usernameFound: false });
    });

    console.log(isValid);
    if (!isValid.usernameFound && isValid.match) {
      console.log('first');
      setConfirmPassword('');
      setUserName('');
      setPassword('');
    }
  };

  useEffect(() => {}, [password, confirmPassword, isValid]);

  return (
    <form
      className='form needs-validation shadow border border-dark'
      onSubmit={formSubmit}
    >
      <h1 className='text-center mb-4 font-monospace'>REGISTRATION PAGE</h1>
      <div className='row mb-3 '>
        <label htmlFor='inputEmail3' className='col-form-label'>
          Username
        </label>
        <div className='col-sm-12'>
          <input
            type='text'
            className='form-control'
            id='inputEmail3'
            onChange={usernameHandler}
            value={username}
            required
          />
        </div>

        <div className='invalid-feedback d-block'>
          {isValid.usernameFound
            ? 'Username is taken'
            : username.trim() === '' &&
              'Please enter valid username/password cannot be blank'}
        </div>
      </div>

      <div className='row mb-3'>
        <label htmlFor='password' className='col-form-label'>
          Password
        </label>
        <div className='col-sm-12'>
          <input
            type='password'
            className='form-control'
            id='password'
            onChange={passwordHandler}
            value={password}
            required
          />
          <div className='invalid-feedback d-block'>
            {!isValid.valid
              ? ' Please enter valid username/password cannot be blank'
              : password.trim() !== '' && ''}
          </div>
        </div>
      </div>

      <div className='row mb-3'>
        <label htmlFor='confirmPassword' className='col-form-label'>
          Confirm Password
        </label>
        <div className='col-sm-12'>
          <input
            type='password'
            className='form-control'
            id='confirmPassword'
            onChange={passwordHandler}
            value={confirmPassword}
            required
          />

          {!isValid.valid && (
            <div className='invalid-feedback d-block'>
              Please enter valid username/password cannot be blank
            </div>
          )}

          {!isValid.match && isValid.valid ? (
            <div className='invalid-feedback d-block'>
              Those passwords didn't match. Please try again.
            </div>
          ) : (
            isValid.match && isValid.isValid && ''
          )}
        </div>
      </div>

      <div className='d-grid gap-2 mt-5'>
        <button type='submit' className='btn btn-dark btn-lg'>
          REGISTER
        </button>
      </div>
    </form>
  );
}

export default Form;


*/

// SECTION: ERROR in condion
/*  NEW

import React from 'react';
import './Form.css';
import { useState } from 'react';
import { useEffect } from 'react';
import Text from '../Input/Text';

function Form() {
  const [username, setUserName] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [isUsernameFound, setIsUsernameFound] = useState(false);
  const [isPasswordValid, setIsPasswordValid] = useState(false);
  const [passwordMatch, setPasswordMatch] = useState(false);
  const [submit, setSubmit] = useState(false);
  const [error, setError] = useState(false);
  const [passwordValidator, setPasswordValidator] = useState({
    
  })

  const registeredUsers = [
    { username: 'Juan' },
    { username: 'upliftcodecamp' },
  ];

  const usernameHandler = (event) => {
    setUserName(event.target.value);

    // username.length > 0 &&
  };

  const passwordHandler = (event) => {
    const passwordInput = event.target.value.trim();
    const passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!@#?]).{8,}$/;
    const passwordCheck = passwordRegex.test(passwordInput);

    if (event.target.id === 'password') {
      setPassword(passwordInput);
    } else {
      setConfirmPassword(passwordInput);
    }

    if (passwordCheck) {
      setIsPasswordValid(true);
    } else {
      setIsPasswordValid(false);
    }
  };

  const formSubmit = (event) => {
    event.preventDefault();

    if (
      password.length !== 0 &&
      confirmPassword.length !== 0 &&
      username.length !== 0
    ) {
     
      if (password === confirmPassword) {
        setPasswordMatch(true);

        const checkIfUserExist = registeredUsers.every(
          (value) => value.username !== username
        );

        if (checkIfUserExist) {
          alert('Successful');

          setConfirmPassword('');
          setUserName('');
          setPassword('');
          setPasswordMatch(null);
          setIsUsernameFound(false);
          setIsPasswordValid(null);
          setSubmit(false);
        } else {
          setIsUsernameFound(true);
        }
      } else {
        setSubmit(true);
        setPasswordMatch(false);
      }
    } else {
      setError(true);
      console.log(error);
    }
  };

  useEffect(() => {}, [
    password,
    passwordMatch,
    username,
    isUsernameFound,
    confirmPassword,
    isPasswordValid,
    submit,
    error,
  ]);

  return (
    <form
      className='form needs-validation shadow border border-dark'
      onSubmit={formSubmit}
    >
      <h1 className='text-center mb-4 font-monospace'>REGISTRATION PAGE</h1>
      <div className='row mb-3 '>
        <Text
          content='Username'
          type='text'
          inputClass='form-control'
          id='inputEmail3'
          onChange={usernameHandler}
          value={username}
          labelFor='inputEmail3'
          labelClassName='col-form-label'
        />
        <div className='invalid-feedback d-block'>
          {isUsernameFound && 'Username is taken'}

          {error && 'Please enter valid username/password cannot be blank.'}
        </div>
      </div>

      <div className='row mb-3'>
        <Text
          content='Password'
          type='password'
          inputClass='form-control'
          id='password'
          onChange={passwordHandler}
          value={password}
          labelFor='password'
          labelClassName='col-form-label'
        />

        <div className='invalid-feedback d-block'>
          {error && 'Please enter valid username/password cannot be blank.'}
        </div>
      </div>

      <div className='row mb-3'>
        <Text
          content=' Confirm Password'
          type='password'
          inputClass='form-control'
          id='confirmPassword'
          onChange={passwordHandler}
          value={confirmPassword}
          labelFor='confirmPassword'
          labelClassName='col-form-label'
        />

        <div className='invalid-feedback d-block'>
          {error && 'Please enter valid username/password cannot be blank.'}

          {!passwordMatch &&
            submit &&
            "The passwords didn't match. Please try again."}
        </div>
      </div>

      <div className='d-grid gap-2 mt-5'>
        <button type='submit' className='btn btn-dark btn-lg'>
          REGISTER
        </button>
      </div>
    </form>
  );
}

export default Form;

*/
