import React from 'react';
import './Form.css';
import { useState } from 'react';
import { useEffect } from 'react';
import Text from '../Input/Text';

function Form({ onClick }) {
  const [username, setUserName] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [isUsernameFound, setIsUsernameFound] = useState(false);
  const [isPasswordValid, setIsPasswordValid] = useState(null);
  const [passwordMatch, setPasswordMatch] = useState(null);
  const [submit, setSubmit] = useState(false);
  const [error, setError] = useState(false);
  const [showPassword, setShowPassword] = useState(false);
  const [showConfirmPassword, setShowConfirmPassword] = useState(false);
  const icon = true;

  const [errorRemover, setErrorRemover] = useState({
    username: null,
    password: null,
    confirmPassword: null,
  });

  const registeredUsers = [
    { username: 'Juan' },
    { username: 'upliftcodecamp' },
  ];

  const showPasswordHandler = () => {
    setShowPassword((arg) => !arg);
  };

  const showConfirmPaswordHandler = () => {
    setShowConfirmPassword((arg) => !arg);
  };

  const usernameHandler = (event) => {
    setUserName(event.target.value);

    if (event.target.value.length > 0) {
      setErrorRemover({ ...errorRemover, username: true });
    }
  };

  const passwordHandler = (event) => {
    const passwordInput = event.target.value.trim();

    if (event.target.id === 'password') {
      setPassword(passwordInput);

      if (passwordInput.length > 0) {
        setErrorRemover({ ...errorRemover, password: true });
      }
    } else {
      setConfirmPassword(passwordInput);
      if (passwordInput.length > 0) {
        setErrorRemover({ ...errorRemover, confirmPassword: true });
      }
    }
  };

  const formSubmit = (event) => {
    event.preventDefault();

    setShowConfirmPassword(false);

    if (
      confirmPassword.length === 0 &&
      password.length === 0 &&
      username.length === 0
    ) {
      setSubmit(true);
      setError(true);
    } else {
      setError(false);

      const passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!@#?]).{8,}$/;
      const passwordCheck = passwordRegex.test(password);

      if (passwordCheck) {
        setIsPasswordValid(true);
      } else {
        setIsPasswordValid(false);
      }

      if (password === confirmPassword && isPasswordValid) {
        setPasswordMatch(true);

        const checkIfUserExist = registeredUsers.every(
          (value) => value.username !== username
        );

        if (checkIfUserExist) {
          onClick(true);

          setConfirmPassword('');
          setUserName('');
          setPassword('');
          setPasswordMatch(null);
          setIsUsernameFound(false);
          setIsPasswordValid(null);
          setSubmit(false);
          setShowPassword(false);
        } else {
          setIsUsernameFound(true);
        }
      } else {
        setSubmit(true);
        setPasswordMatch(false);
      }
    }
  };

  useEffect(() => {}, [
    password,
    passwordMatch,
    username,
    isUsernameFound,
    confirmPassword,
    isPasswordValid,
    error,
    errorRemover,
    showPassword,
  ]);

  return (
    <form
      className='form needs-validation shadow border border-dark'
      onSubmit={formSubmit}
    >
      <h1 className='text-center mb-4 font-monospace'>REGISTRATION PAGE</h1>
      <div className='row mb-3 '>
        <Text
          content='Username'
          type='text'
          inputClass='form-control'
          id='inputEmail3'
          onChange={usernameHandler}
          value={username}
          labelFor='inputEmail3'
          labelClassName='col-form-label'
        />
        <div className='invalid-feedback d-block'>
          {isUsernameFound && 'Username is taken'}

          {error &&
            submit &&
            !errorRemover.username &&
            'Please enter valid username/password cannot be blank.'}
        </div>
      </div>

      <div className='row mb-3'>
        <Text
          content='Password'
          type={showPassword ? 'text' : 'password'}
          inputClass='form-control'
          id='password'
          onChange={passwordHandler}
          value={password}
          labelFor='password'
          labelClassName='col-form-label'
          onClick={showPasswordHandler}
          icon={icon}
        />

        <div className='invalid-feedback d-block'>
          {error &&
            submit &&
            !errorRemover.password &&
            'Please enter valid username/password cannot be blank.'}

          {!isPasswordValid &&
            !error &&
            submit &&
            'Password needs to contain atleast 1 uppercase, 1 lowercase, 1 number and a special character'}
        </div>
      </div>

      <div className='row mb-3'>
        <Text
          content=' Confirm Password'
          type={showConfirmPassword ? 'text' : 'password'}
          inputClass='form-control'
          id='confirmPassword'
          onChange={passwordHandler}
          value={confirmPassword}
          labelFor='confirmPassword'
          labelClassName='col-form-label'
          onClick={showConfirmPaswordHandler}
          icon={icon}
        />

        <div className='invalid-feedback d-block'>
          {error &&
            submit &&
            !errorRemover.confirmPassword &&
            'Please enter valid username/password cannot be blank.'}

          {!isPasswordValid &&
            !error &&
            submit &&
            'Password needs to contain atleast 1 uppercase, 1 lowercase, 1 number and a special character'}

          {!passwordMatch &&
            submit &&
            !error &&
            isPasswordValid &&
            "The passwords didn't match. Please try again."}
        </div>
      </div>

      <div className='d-grid gap-2 mt-5'>
        <button type='submit' className='btn btn-dark btn-lg'>
          REGISTER
        </button>
      </div>
    </form>
  );
}

export default Form;
