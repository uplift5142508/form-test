import { useState } from 'react';
import Container from './components/Container/Container';
import Form from './components/Form/Form';
import Modal from './components/Modal/Modal';

function App() {
  const [show, setShow] = useState(false);

  const showModal = (params) => {
    setShow(params);
  };

  return (
    <Container>
      {show && <Modal onClick={showModal} />}
      <Form onClick={showModal} />
    </Container>
  );
}

export default App;
