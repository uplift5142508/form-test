//SECTION: First idea
for (let i in arr) {
  // console.log(arr[i].length);
  // console.log(arr[arr.length - 1].length);

  //   Gets the length of the first inner array element  and checks if it's equals to the last inner array element's length

  // Loophole: Error with this one is that, if the middle element of the array is NOT the same as the current element then this line of code will NEVER execute
  if (arr[i].length === arr[arr.length - 1].length) {
    //   console.log(arr[arr.length - 1].length);

    //   console.log(arr[arr.length - 1]);
    // Checks if the outer array length is NOT equals to the last inner array element length
    if (arr.length !== arr[arr.length - 1].length) {
      return 'Invalid input';
    }
  }
}

// SECTION: Second idea
for (let i = 0; i < arr.length; i++) {
  // Get the length of the current element in array (index:0)
  let currentEl = arr[i].length;
  //   Get the length of the next element in the array (index: 0 + 1), 0 because its the current element that we're currently on and by adding +1, we get the 2nd element which has an index of 0
  let nextEl = arr[i + 1].length;

  /* 
    We'll compare if the length of the current element in the array is greater than the next element in the array OR check if the current element in the array is less than the next element in the array, if so, then we'll return "Invalid input"
    
    testArray1 = [
        [10, 20, 40],
        [40, 50, 60],
        [70, -80,40],
    ]

    Loop hole: With this solution, if index 0 which has the length of 3 has the same length as the next element then it will result to false which executes the else statement which isn't the behaviour we want

    P.S Further explanation on Excalidraw
*/
  if (currentEl > nextEl || currentEl < nextEl) {
    return 'Invalid input';
  } else {
    console.log(currentEl);
    console.log(nextEl);
    return 'Same length';
  }
}

// SECTION: Third idea - Working logic
const getGreatestSum = (arr) => {
  console.log(arr);

  // Declared an empty array which will hold the length of each element in the inner array
  const test = [];

  // Loop through each individual array and add its length to the newly declared array
  for (let i = 0; i < arr.length; i++) {
    test.push(arr[i].length);
  }

  // Sort the array in order to check if in ANY of the elements have a lower value then the next
  const test2 = test.sort((a, b) => a - b);

  console.log(test2);

  // Check if the length of first element of the array is NOT equals to the length of the second element in the array
  // Loophole: This doesn't check IF the outer element has the same length as the inner element. It only checks if the first element is not equals to the second element. However, what if the second element has the same length as the first element BUT the last element has more element than the first 2 elements?
  // Solution:  test2[0] !== test2[test2.length -1]
  // Anoter loophole in the solution above: Now we're only checking the first element of the inner array and the last element of the inner array, the next condition that we need to check is that, IF the elements of the inner array is EQUALS to the outer element length (e.g. [2,2,3] , [2,2,3]), looking at the elements, we have inner length of 3 BUT the outer length is 2. This should return false because the inner length of the array should be EQUALS to the outer length of the array
  // Solution: test2[0] !== test2[test2.length - 1] || arr[i].length !== arr.length
  // The solution above now checks the first element of the sorted array and the last element of the sorted array are equals. We're using OR operator because we want to check if the first element of the array is not equals to the last element length of the array OR if the outer length of the array is NOT equals to the inner length of the array.
  if (test2[0] !== test2[1]) {
    //   If it isn't the same then return Invalid
    return 'Invalid';
  } else {
    //   If it is the same then return "Test"
    return 'Test';
  }

  // console.log(arr[i].length !== arr.length);
  // console.log(
  //   test2[0] !== test2[test2.length - 1] || test2.length !== arr.length
  // );
  //   console.log(arr[i].length !== arr.length);
  //   console.log(
  //     test2[0] !== test2[test2.length - 1] || test2.length !== arr.length
  //   );
  // if (test2[0] !== test2[test2.length - 1]) {
  //   return 'Invalid';
  // } else {
  //   return 'Test';
  // }
};

// SECTION: Solution four
const getGreatestSum4 = (arr) => {
  console.log(arr);
  // Compare inner array length

  const arrLength = [];

  for (let i = 0; i < arr.length; i++) {
    arrLength.push(arr[i].length);
  }

  const sortedArrLength = arrLength.sort((a, b) => a - b);

  const sum = [];
  let sumY = [];

  for (let i = 0; i < sortedArrLength.length; i++) {
    if (
      sortedArrLength[0] !== sortedArrLength[sortedArrLength.length - 1] ||
      arr[i].length !== arr.length
    ) {
      return 'Invalid';
    } else {
      // Push the sum of all elements in the outer array. e.g. [[1,2,3], [4,5,6]], it will sum 1+2+3 = 6. Then it will sum 4+5+6
      sum.push(arr[i].reduce((prev, curr) => prev + curr));

      // column sum
      // console.log(`${i}: ${arr[i]}`);

      // Loop through the inner elements of the array e.g. [[1,2,3], [4,5,6]], in this instance, it will loop through 1,2,3 and 4,5,6
      for (let j = 0; j < arr[i].length; j++) {
        // console.log(`j[${j}]: ${arr[i][j]}`);

        /*
          Check if the current index (i) is 0, because 0 is the outer index of the elements

          For instance, check the testArray1 in the code below. We're accessing the current index 0 which is [10,20,40].
          Now, we're accessing its INNER elements which are the individual elements of the inner array e.g. 10,20,40. We're
          assigning sumY's inner elements the values of the current inner elements, basically, we're assigning it the values [10,20,40].

          In the else statement, IF the the index (i) is not the first index then add the current value its currently holding to the
          next inner index.

          if:
          sumY[j] = arr[i][j]  ---> sumY[0] = arr[10,20,40]

          else:
          sumY[1] = arr[40, 50, 60]  ---> sumY[1] = arr[10+40=50, 20+50=70, 40+60=100]
          sumY[2] = arr[70,-80,90] ---> sumY[1] = arr[50+70=120, 70+-80=-10, 100+90=190]
        */
        console.log(sum[j]);
        if (i === 0) {
          sumY[j] = arr[i][j];
        } else {
          sumY[j] += arr[i][j];
        }
        // console.log(sumY);
      }
      //   console.log(sumY.reduce((prev, current) => prev + current));
    }
  }

  // Sorting out the X and Y axis first in order to identify which element has the highest number
  const highestSumY = sumY.sort((a, b) => a - b);
  const highestSumX = sum.sort((a, b) => a - b);

  // Using ternary operator, return highestSumX if it's greater than highestSumY, otherwise return highestSumY
  return highestSumX[highestSumX.length - 1] >
    highestSumY[highestSumY.length - 1]
    ? highestSumX[highestSumX.length - 1]
    : highestSumY[highestSumY.length - 1];
};

const testArray1 = [
  [10, 20, 40],
  [40, 50, 60],
  [70, -80, 90],
];

// let arr = [
//   [1, 2, 3],
//   [4, 5, 6],
//   [7, 8, 9],
// ];

// let colSums = Array(arr[0].length).fill(0); // initialize the sum for each column to zero

// for (let i = 0; i < arr.length; i++) {
//   for (let j = 0; j < arr[i].length; j++) {
//     colSums[j] += arr[i][j]; // add the value of the current column to the corresponding sum
//   }
// }

// console.log(colSums); // prints an array containing the sum for each column

let arr = [
  [1, 2, 3],
  [4, 5, 6],
  [7, 8, 9],
];

let colSums = []; // create an empty array to store the sum for each column

// iterate through each inner array
for (let i = 0; i < arr.length; i++) {
  // iterate through each column in the current inner array
  for (let j = 0; j < arr[i].length; j++) {
    // if this is the first inner array, create a new sum for each column
    if (i === 0) {
      colSums[j] = arr[i][j]; // set the initial sum to the value of the current element
    } else {
      colSums[j] += arr[i][j]; // add the current element to the existing sum for the current column
    }
  }
}

console.log(colSums); // prints an array containing the sum for each column
