// Access each element in x axis
// Access each element in y axis
// IF x and y axis in the array does NOT have the same length then return "Invalid Input"
// Find sum of row array
// Find sum of column array
// Conditional statement to find which sum in x or y axis has the highest

const getGreatestSum = (arr) => {
  const arrLength = [];

  for (let i = 0; i < arr.length; i++) {
    arrLength.push(arr[i].length);
  }

  const sortedArrLength = arrLength.sort((a, b) => a - b);

  const sum = [];
  let sumY = [];

  for (let i = 0; i < sortedArrLength.length; i++) {
    if (
      sortedArrLength[0] !== sortedArrLength[sortedArrLength.length - 1] ||
      arr[i].length !== arr.length
    ) {
      return 'Invalid';
    } else {
      sum.push(arr[i].reduce((prev, curr) => prev + curr));

      for (let j = 0; j < arr[i].length; j++) {
        if (i === 0) {
          sumY[j] = arr[i][j];
        } else {
          sumY[j] += arr[i][j];
        }
      }
    }
  }

  const highestSumY = sumY.sort((a, b) => a - b);
  const highestSumX = sum.sort((a, b) => a - b);

  return highestSumX[highestSumX.length - 1] >
    highestSumY[highestSumY.length - 1]
    ? highestSumX[highestSumX.length - 1]
    : highestSumY[highestSumY.length - 1];
};

const testArray1 = [
  [10, 20, 40],
  [40, 50, 60],
  [70, -80, 90],
];
const testArray2 = [
  [10, 20, 40],
  [40, 50, 90],
  [70, 80, 10],
];
const testArray3 = [
  [10, 15, 22],
  [34, 50],
  [1, 78, 9],
];

const testArray4 = [
  [10, 23, -2],
  [4, 50, 87],
];

console.log(getGreatestSum(testArray1));
console.log(getGreatestSum(testArray2));
console.log(getGreatestSum(testArray3));
console.log(getGreatestSum(testArray4));
